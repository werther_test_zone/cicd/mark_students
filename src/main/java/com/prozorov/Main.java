package com.prozorov;

import java.util.ArrayList;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import org.bson.Document;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        int requiredMark = 2;
        ArrayList <String> names = new ArrayList<>();

        System.out.println(parseArrayList(getStudentsList(names, requiredMark)));
    }


    public static String getUri(){
        String uri = "mongodb://";
        uri += System.getenv("MONGO_USERNAME");
        uri += ":";
        uri += System.getenv("MONGO_PASSWORD");
        uri += "@";
        uri += System.getenv("MONGO_HOST");
        return uri;
    }

    public static ArrayList <String> getStudentsList(ArrayList <String> names ,int requiredMark){
        try (MongoClient mongoClient = new MongoClient(new MongoClientURI(getUri()))) {
            MongoDatabase database = mongoClient.getDatabase(System.getenv("MONGO_DB_NAME"));
            MongoCollection<Document> collection = database.getCollection(System.getenv("MONGO_COLLECTION_NAME"));

            Document searchQuery = new Document();
            searchQuery.put("Mark", requiredMark);
            FindIterable<Document> cursor = collection.find(searchQuery);

            try (final MongoCursor<Document> cursorIterator = cursor.cursor()) {
                while (cursorIterator.hasNext()) {
                    names.add(cursorIterator.next().toString());
                }
            }

            return names;
        }
        catch (Exception e){
            System.err.println(e.getMessage());
        }
        return names;
    }

    public static ArrayList<String> parseArrayList(ArrayList<String> ex_list){

        ArrayList<String> new_list = new ArrayList<String>();

        for (int i = 0; i < ex_list.size(); i++) {
            String str = ex_list.get(i);
            String [] params = str.split(", ");
            String full_name_array=params[1];
            String [] name_array = full_name_array.split("=");
            String full_name_str = name_array [1];
            new_list.add(full_name_str);
        }
        return new_list;
    }
}